use std::cmp::{max, Ordering};
use std::mem::replace;

#[derive(Clone, Debug, PartialEq)]
pub struct AvlNode<T: Ord> {
    value: T,
    left: AvlTree<T>,
    right: AvlTree<T>,
    height: usize,
}

impl<T: Ord> AvlNode<T> {
    pub fn new(value: T, left: AvlTree<T>, right: AvlTree<T>) -> Self {
        let n = AvlNode {
            value: value,
            left: left,
            right: right,
            height: 1,
        };
        n
    }

    pub fn from_value(value: T) -> Self {
        AvlNode {
            value: value,
            left: None,
            right: None,
            height: 1,
        }
    }

    pub fn get_value(&self) -> &T {
        &self.value
    }

    pub fn height(&self) -> usize {
        1 + max(
            self.left.as_ref().map_or(0, |node| node.height),
            self.right.as_ref().map_or(0, |node| node.height),
        )
    }

    pub fn left_height(&self) -> usize {
        self.left.as_ref().map_or(0, |left| left.height)
    }

    pub fn right_height(&self) -> usize {
        self.right.as_ref().map_or(0, |right| right.height)
    }

    pub fn balance_factor(&self) -> i8 {
        let left_hgt = self.left_height();
        let right_hgt = self.right_height();

        if left_hgt >= right_hgt {
            (left_hgt - right_hgt) as i8
        } else {
            -((right_hgt - left_hgt) as i8)
        }
    }

    pub fn update_height(&mut self) {
        self.height = 1 + max(self.left_height(), self.right_height());
    }

    pub fn rot_right(&mut self) -> bool {
        if self.left.is_none() {
            return false;
        }

        let left_right_tree = self.left.as_mut().unwrap().right.take();
        let new_root = *replace(&mut self.left, left_right_tree).unwrap();
        let old_root = replace(self, new_root);

        replace(&mut self.right, Some(Box::new(old_root)));

        if let Some(node) = self.right.as_mut() {
            node.update_height();
        }
        self.update_height();

        true
    }

    pub fn rot_left(&mut self) -> bool {
        if self.right.is_none() {
            return false;
        }

        let right_left_tree = self.right.as_mut().unwrap().left.take();
        let new_root = *replace(&mut self.right, right_left_tree).unwrap();
        let old_root = replace(self, new_root);

        replace(&mut self.left, Some(Box::new(old_root)));

        if let Some(node) = self.left.as_mut() {
            node.update_height();
        }
        self.update_height();

        true
    }

    fn rebalance(&mut self) -> bool {
        match self.balance_factor() {
            -2 => {
                let right_node = self.right.as_mut().unwrap();
                if right_node.balance_factor() == 1 {
                    right_node.rot_right();
                }
                self.rot_left();
                true
            }

            2 => {
                let left_node = self.left.as_mut().unwrap();
                if left_node.balance_factor() == -1 {
                    left_node.rot_left();
                }
                self.rot_right();
                true
            }

            _ => false,
        }
    }
}

pub type AvlTree<T> = Option<Box<AvlNode<T>>>;

#[derive(Clone, Debug, PartialEq)]
pub struct AvlTreeSet<T: Ord> {
    root: AvlTree<T>,
}

impl<T: Ord> AvlTreeSet<T> {
    pub fn new() -> Self {
        Self { root: None }
    }

    pub fn insert(&mut self, value: T) -> bool {
        let mut curr_tree = &mut self.root;
        let mut prev_ptrs = Vec::<*mut AvlNode<T>>::new();

        while let Some(curr_node) = curr_tree {
            prev_ptrs.push(&mut **curr_node);

            match curr_node.value.cmp(&value) {
                Ordering::Less => curr_tree = &mut curr_node.right,
                Ordering::Equal => {
                    return false;
                }
                Ordering::Greater => curr_tree = &mut curr_node.left,
            }
        }

        *curr_tree = Some(Box::new(AvlNode::from_value(value)));

        for ptr in prev_ptrs.into_iter().rev() {
            let node = unsafe { &mut *ptr };
            node.update_height();
            node.rebalance();
        }
        true
    }
}

#[derive(Debug)]
pub struct AvlTreeSetIter<'a, T: Ord> {
    prev_nodes: Vec<&'a AvlNode<T>>,
    curr_tree: &'a AvlTree<T>,
}

impl<'a, T: 'a + Ord> Iterator for AvlTreeSetIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match *self.curr_tree {
                None => match self.prev_nodes.pop() {
                    None => {
                        return None;
                    }
                    Some(ref prev_node) => {
                        self.curr_tree = &prev_node.right;
                        return Some(&prev_node.value);
                    }
                },

                Some(ref curr_node) => {
                    if curr_node.left.is_some() {
                        self.prev_nodes.push(&curr_node);
                        self.curr_tree = &curr_node.left;
                        continue;
                    }

                    if curr_node.right.is_some() {
                        self.curr_tree = &curr_node.right;
                        return Some(&curr_node.value);
                    }

                    self.curr_tree = &None;
                    return Some(&curr_node.value);
                }
            }
        }
    }
}

impl<'a, T: 'a + Ord> AvlTreeSet<T> {
    // Using impl Traits
    pub fn iter(&'a self) -> impl Iterator<Item = &'a T> + 'a {
        self.node_iter().map(|node| &node.value)
    }

    pub fn node_iter(&'a self) -> impl Iterator<Item = &'a AvlNode<T>> + 'a {
        AvlTreeSetNodeIter {
            prev_nodes: Vec::default(),
            current_tree: &self.root,
        }
    }
}

impl<T: Ord> std::iter::FromIterator<T> for AvlTreeSet<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut set = Self::new();
        for i in iter {
            set.insert(i);
        }
        set
    }
}

#[derive(Debug)]
pub struct AvlTreeSetNodeIter<'a, T: Ord> {
    prev_nodes: Vec<&'a AvlNode<T>>,
    current_tree: &'a AvlTree<T>,
}

impl<'a, T: 'a + Ord> Iterator for AvlTreeSetNodeIter<'a, T> {
    type Item = &'a AvlNode<T>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match *self.current_tree {
                None => match self.prev_nodes.pop() {
                    None => {
                        return None;
                    }

                    Some(ref prev_node) => {
                        self.current_tree = &prev_node.right;

                        return Some(prev_node);
                    }
                },

                Some(ref current_node) => {
                    if current_node.left.is_some() {
                        self.prev_nodes.push(&current_node);
                        self.current_tree = &current_node.left;

                        continue;
                    }

                    if current_node.right.is_some() {
                        self.current_tree = &current_node.right;

                        return Some(current_node);
                    }

                    self.current_tree = &None;

                    return Some(current_node);
                }
            }
        }
    }
}